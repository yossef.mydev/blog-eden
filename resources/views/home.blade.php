@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  <div class="container">
                      <h1>Posts</h1>
                      <ul>
                            @foreach ($posts as $post)
                          <li> <a href="{{ 'post/'. $post->title }}">
                             {{$post->title}}
                          </li>
                          @endforeach
                      </ul>

                      @auth
                @if (Auth::user()->is_dev)
                 <p> <a href="/addpost"> Add a post </p>

                   @endif
                   @endauth
                </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
