@extends('layouts.app')

@section('content')


<div class="container">
    <form action="/home" method="POST">
        @csrf
<div class="form-group">

     <label for="title">Add a post</label>
     @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
      <input type="text" name='title' class="form-control" id="title" aria-describedby="emailHelp" placeholder="Enter the title">
<br>
@error('content')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
      <textarea class="form-control" name='content' id="content" rows="7" placeholder="Enter the content"></textarea>
 <br>
 @error('technical_content')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
      <textarea class="form-control" name='technical_content'  id="technical_content" rows="7" placeholder="Enter the technical content"></textarea>

    </div>
    <br>
    <div class="form-group">
            <label for="title">Select a tag</label>
    </div>

    <br>
    <input type="submit" class="btn btn-primary" value="Add the post">
</form>
    </div>
@endsection
