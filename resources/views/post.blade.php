@extends('layouts.app')

@section('content')
<div class="container">
<h1>{{$post->title}}</h1>
<div class="container">
      <h5> {{$post->content}}</h5>
@auth
@if (Auth::user()->is_dev)
 <h5> {{$post->technical_content}}</h5>

   @endif
   @endauth
</div>
</div>
@endsection
