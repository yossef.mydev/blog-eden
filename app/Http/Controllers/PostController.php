<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{


    public function addPost(){

       return view('addpost');
    }

    public function postView(){

        $post = DB::table('posts')->where('title',request()->title)->first();
        return view('post',['post'=> $post]);
    }

    public function  savePost(Request $request){



        $validator = Validator::make(request()->all(), [
            'title' => 'required|unique:posts|max:255',
            'content' => 'required|min:10',
            'technical_content' => 'required|min:10',
        ]);

      


        if ($validator->fails()) {
            return redirect('addpost')
                        ->withErrors($validator)
                        ->withInput();
        }

        // there is not errors

       Post::create([
            'title'=>request('title'),
            'content'=>request('content'),
            'technical_content'=>request('technical_content'),
            'likes_count'=>0,
        ]);

        return redirect('home');
        }

    public function postByTag(){
//
    }

}



/*
       Route::get('/addpost', 'PostController@addPost');
       Route::get('/post/{name}', 'PostController@postView');
       Route::get('/posts', 'PostController@postByTag');

*/
